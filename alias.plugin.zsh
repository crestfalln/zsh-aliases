alias yay=paru
alias c=clear
alias vim=nvim
alias cat=bat
alias top=btop
alias bpytop=btop
alias get='aria2c -x 10'
alias open='xdg-open'
alias yank='xclip -selection clipboard'
alias lvim=lunar-neovide.sh
alias hd=xxd
